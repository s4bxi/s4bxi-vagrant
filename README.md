# S4BXI's Vagrant configuration

This repo holds a [Vagrant](https://www.vagrantup.com) +
[Ansible](https://www.ansible.com) configuration to install S4BXI quickly. It
only requires Vagrant and a [provider](https://www.vagrantup.com/docs/providers)
(such as Virtualbox, VMware, Docker, etc.) to function

## Usage

Start the VM : `vagrant up`

Log in SSH : `vagrant ssh`

Stop the VM : `vagrant halt`

### Transfer files between the VM and the host :

The easiest way is to use the folder `workdir` on the host machine and
`/vagrant/workdir` on the VM, since they are synchronized.

Another way is to transfer files through SSH (using rsync for example), with the
following parameters :

```
ssh vagrant@127.0.0.1 -p 2222 -i .vagrant/machines/default/virtualbox/private_key
```

## Additional info

The configuration is based on CentOS 8, but S4BXI is compatible with any Linux
distribution

Inside the VM, SimGrid and S4BXI are installed in `/opt/simgrid` and
`/opt/s4bxi` respectively, and their source code can be found in `/opt/src`
(these folders can be customized in `ansible/vars.yml` before provisioning the VM)